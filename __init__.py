# coding=UTF-8

#The MIT License (MIT)
#Copyright (c) 2012 Reto Seeholzer
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import SCons.Builder
import sys
import os
import re
import fnmatch

#this SCons tool provides support for the Helsinki Finite-State Transducer Technology (HFST).
#these command line tools are supported by this version:


#hfst-calculate
#hfst-compose
#hfst-compose-intersect
#hfst-concatenate
#hfst-conjunct
#hfst-determinize
#hfst-disjunct
#hfst-fst2fst
#hfst-fst2strings
#hfst-fst2txt
#hfst-invert
#hfst-lexc
#hfst-minimize
#hfst-project
#hfst-push-weights
#hfst-regexp2fst
#hfst-remove-epsilons
#hfst-repeat
#hfst-reverse
#hfst-sfstpl2fst
#hfst-substitute
#hfst-subtract
#hfst-twolc
#hfst-xfst


#functions

def perl(target,source,env):
	#at least one source is required
	if len(source) == 0:
		print >> sys.stderr, "***ERROR: No source file given.\n"
		return 1
	#the first source has to be a perl script
	perl_file = str(source.pop(0))
	if not fnmatch.fnmatch(perl_file,'*.pl'):
		print >> sys.stderr, "***WARNING: "+perl_file+" does not end with the default ending for perl files. The first source file has to be a perl file. The Builder will fail otherwise.\n"
	if len(source) == 0:
		print >> sys.stderr, "***WARNING: Not giving a file to the perl script might make it read its input form the shell, stopping your build process.\n"
	#building the command	
	source_files = ''
	for i in source:
		source_files = source_files + str(i) + ' '
	command = 'perl '+ perl_file + ' ' + source_files + ' > ' + str(target[0])
	#executing the command	
	print >> sys.stderr, "***INFO: now executing: \n" + env.subst(command) + '\n'
	return os.system(env.subst(command))


def cat(target,source,env):
	#at least one source is required
	if len(source) == 0:
		print >> sys.stderr, "***ERROR: No source file given.\n"
		return 1
	#building the command
	command = 'cat '
	for i in source:
		command = command + str(i) + ' '
	command = command + '> ' + str(target[0])
	#executing the command
	print >> sys.stderr, "***INFO: now executing: \n" + env.subst(command) + '\n'
	return os.system(env.subst(command))

def xfst(target,source,env):
	#SCons manages the number of sources for this Builder.
	source = str(source[0])
	#as the target is defined in the source file, the file is altered
	#a temporary file is created, containing the changes
	tmp_file = os.path.dirname(source)+"/tmp_"+os.path.basename(source)
	if os.path.exists(tmp_file):
		os.remove(tmp_file)
	stack_save = False
	ins = open(source,"r")
	outs = open(tmp_file,"w")
	#the source file is parsed line by line
	#the line "save stack" gets adjusted
	for line in ins:
		if fnmatch.fnmatch(line,'save stack *'):
			print >>outs, "save stack "+str(target[0])+"\n"
			stack_save = True
		else:
			print >>outs, line.rstrip('\n')
	if stack_save == False:
		print >>outs, "save stack "+str(target[0])+"\n"
	ins.close()
	outs.close()
	#building the command
	if env.Detect("hfst-xfst"):
		command = "hfst-xfst "
	elif env.Detect("foma"):
		command = "foma "
	else:
		print >> sys.stderr, "***ERROR: neither hfst-xfst nor foma was found on the system.\n"
		return(1)
	command = command+'$HFSTFLAGS $XFSTFLAGS '+tmp_file
	#executing the command and deliting the temporary file
	print >> sys.stderr, "***INFO: now executing: \n" + env.subst(command) + '\n'
	return_value = os.system(env.subst(command))
	if os.path.exists(tmp_file):
		os.remove(tmp_file)
	return return_value
			

def Binary_HFST_Operator(operator,target,source,env):
	if len(source) != 2:
		print >> sys.stderr, "***ERROR: This Builder takes exactly 2 sources.\n"
		return 1
	command = operator + ' '
	Arg1 = str(source[0])
	Arg2 = str(source[1])
	command = command + Arg1 + ' ' + Arg2 + ' > ' + str(target[0])
	print >> sys.stderr, "***INFO: now executing: \n" + env.subst(command) + '\n'
	return os.system(env.subst(command))		

def hfst_compare(target,source,env):
	Binary_HFST_Operator(\
	'hfst-compare $HFSTFLAGS $COMPAREFLAGS',\
	target,source,env)

def hfst_compose(target,source,env):
	Binary_HFST_Operator(\
	'hfst-compose $HFSTFLAGS $COMPOSEFLAGS',\
	target,source,env)

def hfst_compose_intersect(target,source,env):
	Binary_HFST_Operator(\
	'hfst-compose-intersect $HFSTFLAGS $COMPOSEINTERSECTFLAGS',\
	target,source,env)

def hfst_concatenate(target,source,env):
	Binary_HFST_Operator(\
	'hfst-concatenate $HFSTFLAGS $CONCATENATEFLAGS',\
	target,source,env)

def hfst_conjunct(target,source,env):
	Binary_HFST_Operator(\
	'hfst-conjunct $HFSTFLAGS $CONJUNCTFLAGS',\
	target,source,env)

def hfst_disjunct(target,source,env):
	Binary_HFST_Operator(\
	'hfst-disjunct $HFSTFLAGS $DISJUNCTFLAGS',\
	target,source,env)

def hfst_subtract(target,source,env):
	Binary_HFST_Operator('hfst-subtract',target,source,env)

#Scanner

#SFST Scanner
sfst_include_sfst_re = re.compile(r'^#include\s+"(\S+)"',re.M)
sfst_include_txt_re =  re.compile(r'"(\S+)"',re.M)
sfst_include_comp_re =  re.compile(r'"<(\S+)>"',re.M)

def sfst_scan(node,env,path):
	contents = node.get_text_contents()
	includes_sfst = sfst_include_sfst_re.findall(contents)
	includes_txt = sfst_include_txt_re.findall(contents)
	includes_comp = sfst_include_comp_re.findall(contents)
	#return env.File(includes_sfst + includes_txt + includes_comp,node.dir)
	return env.File(includes_sfst + includes_txt + includes_comp)

sfst_scan = SCons.Scanner.Scanner(function = sfst_scan,skeys = ['.sfst'], recursive=True)

#Xfst Scanner
xfst_include_properties_re = re.compile(r'add properties < (\S+)',re.M)
xfst_include_properties2_re =  re.compile(r'read properties < (\S+)',re.M)

xfst_include_defined_re =  re.compile(r'load defined (\S+)',re.M)

xfst_include_stack_re =  re.compile(r'load stack (\S+)',re.M)
xfst_include_stack2_re =  re.compile(r'read regex \[@bin (\S+)',re.M)

xfst_include_prolog_re =  re.compile(r'read prolog < (\S+)',re.M)
xfst_include_prolog2_re =  re.compile(r'read regex \[@pl (\S+)',re.M)

xfst_include_regex_re =  re.compile(r'read regex < (\S+)',re.M)
xfst_include_regex2_re =  re.compile(r'read regex \[@re (\S+)',re.M)

xfst_include_spaced_text_re =  re.compile(r'read spaced-text < (\S+)',re.M)
xfst_include_spaced_text2_re =  re.compile(r'read regex \[@stxt (\S+)',re.M)

xfst_include_text_re =  re.compile(r'read text < (\S+)',re.M)
xfst_include_text2_re =  re.compile(r'read regex \[@txt "(\S+)"',re.M)

def xfst_scan(node,env,path):
	contents = node.get_text_contents()
	
	includes_properties = xfst_include_properties_re.findall(contents)
	includes_properties2 = xfst_include_properties2_re.findall(contents)
	
	includes_defined = xfst_include_defined_re.findall(contents)
	
	includes_stack = xfst_include_stack_re.findall(contents)
	includes_stack2 = xfst_include_stack2_re.findall(contents)
	
	includes_prolog = xfst_include_prolog_re.findall(contents)
	includes_prolog2 = xfst_include_prolog2_re.findall(contents)
	
	includes_regex = xfst_include_regex_re.findall(contents)
	includes_regex2 = xfst_include_regex2_re.findall(contents)
	
	includes_spaced_text = xfst_include_spaced_text_re.findall(contents)
	includes_spaced_text2 = xfst_include_spaced_text2_re.findall(contents)
	
	includes_text = xfst_include_text_re.findall(contents)
	includes_text2 = xfst_include_text2_re.findall(contents)
	
	return env.File(includes_properties + includes_properties2 + includes_defined + includes_stack + includes_stack2 + includes_prolog + includes_prolog2 + includes_regex + includes_regex2 + includes_spaced_text + includes_spaced_text2 + includes_text + includes_text2)

xfst_scan = SCons.Scanner.Scanner(function = xfst_scan, skeys = ['.xfst'], recursive=True)


# HFST-Builder, which directly call the command line

twolc_builder = SCons.Builder.Builder(action = \
'hfst-twolc $HFSTFLAGS $TWOLCFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.twolc', single_source = True)

lexc_builder = SCons.Builder.Builder(action = \
'hfst-lexc $HFSTFLAGS $LEXCFLAGS -o $TARGET $SOURCE', \
suffix = '.hfst', src_suffix = '.lexc', single_source = True)

calculate_builder = SCons.Builder.Builder(action = \
'hfst-calculate $HFSTFLAGS $CALCULATEFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.sfst', single_source = True, source_scanner = sfst_scan)

invert_builder = SCons.Builder.Builder(action = \
'hfst-invert $HFSTFLAGS $INVERTFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True)

determinize_builder = SCons.Builder.Builder(action = \
'hfst-determinize $HFSTFLAGS $DETERMINIZEFLAGS $SOURCE'\
+' > $TARGET', suffix = '.hfst', src_suffix = '.hfst', single_source = True)

fst2fst_builder = SCons.Builder.Builder(action = \
'hfst-fst2fst $HFSTFLAGS $FST2FSTFLAGS $SOURCE > $TARGET', single_source = True)

fst2strings_builder = SCons.Builder.Builder(action = \
'hfst-fst2strings $HFSTFLAGS $FST2STRINGSFLAGS $SOURCE > $TARGET', \
suffix = '.txt', src_suffix = '.hfst', single_source = True)

fst2txt_builder = SCons.Builder.Builder(action = \
'hfst-fst2txt $HFSTFLAGS $FST2TXTFLAGS $SOURCE > $TARGET', \
suffix = '.att', src_suffix = '.hfst', single_source = True)

head_builder = SCons.Builder.Builder(action = \
'hfst-head $HFSTFLAGS $HEADFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True)

minimize_builder = SCons.Builder.Builder(action = \
'hfst-minimize $HFSTFLAGS $MINIMIZEFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True)

name_builder = SCons.Builder.Builder(action = \
'hfst-name $HFSTFLAGS $NAMEFLAGS $SOURCE > $TARGET', \
src_suffix = '.hfst', single_source = True)

project_builder = SCons.Builder.Builder(action = \
'hfst-project $HFSTFLAGS $PROJECTFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True) 

push_weights_builder = SCons.Builder.Builder(action = \
'hfst-push-weights $HFSTFLAGS $PUSHWEIGHTSFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True)

regexp2fst_builder = SCons.Builder.Builder(action = \
'hfst-regexp2fst $HFSTFLAGS $REGEXP2FSTFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.regex', single_source = True)

remove_epsilons_builder = SCons.Builder.Builder(action = \
'hfst-remove-epsilons $HFSTFLAGS $REMOVEEPSILONSFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True)

repeat_builder = SCons.Builder.Builder(action = \
'hfst-repeat $HFSTFLAGS $REPEATFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True)

reverse_builder = SCons.Builder.Builder(action = \
'hfst-reverse $HFSTFLAGS $REVERSEFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True)

sfstpl2fst_builder = SCons.Builder.Builder(action = \
'hfst-sfstpl2fst $HFSTFLAGS $SFSTPL2FSTFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.sfst', single_source = True, source_scanner = sfst_scan)

strings2fst_builder = SCons.Builder.Builder(action = \
'hfst-strings2fst $HFSTFLAGS $STRINGS2FSTFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.txt', single_source = True)

tail_builder = SCons.Builder.Builder(action = \
'hfst-tail $HFSTFLAGS $TAILFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True)

txt2fst_builder = SCons.Builder.Builder(action = \
'hfst-txt2fst $HFSTFLAGS $TXT2FSTFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.att', single_source = True)

xfst_builder = SCons.Builder.Builder(action = \
xfst, suffix = '.hfst', src_suffix = '.xfst', single_source = True, source_scanner = xfst_scan)

substitute_builder = SCons.Builder.Builder(action = \
'hfst-substitute $HFSTFLAGS $SUBSTITUTEFLAGS $SOURCE > $TARGET', \
suffix = '.hfst', src_suffix = '.hfst', single_source = True)


#HFST-Builder calling another Python function

compare_builder = SCons.Builder.Builder(action = hfst_compare)

compose_builder = SCons.Builder.Builder(action = \
hfst_compose, suffix = '.hfst', src_suffix = '.hfst')

compose_intersect_builder = SCons.Builder.Builder(action = \
hfst_compose_intersect, suffix = '.hfst', src_suffix = '.hfst')

concatenate_builder = SCons.Builder.Builder(action = \
hfst_concatenate, suffix = '.hfst', src_suffix = '.hfst')

conjunct_builder = SCons.Builder.Builder(action = \
hfst_conjunct, suffix = '.hfst', src_suffix = '.hfst')

disjunct_builder = SCons.Builder.Builder(action = \
hfst_disjunct, suffix = '.hfst', src_suffix = '.hfst')

subtract_builder = SCons.Builder.Builder(action = \
hfst_subtract, suffix = '.hfst', src_suffix = '.hfst')


#all the other Builders

perl_builder = SCons.Builder.Builder(action = perl)

cat_builder = SCons.Builder.Builder(action = cat)

#each SCons tool has to provide the generate() function. 
#it adds all the Builders to the environment.

def generate(env):
	env['BUILDERS']['HFST_HEAD'] = head_builder
	env['BUILDERS']['HFST_TWOLC'] = twolc_builder
	env['BUILDERS']['HFST_LEXC'] = lexc_builder
	env['BUILDERS']['HFST_INVERT'] = invert_builder
	env['BUILDERS']['CAT'] = cat_builder
	env['BUILDERS']['PERL'] = perl_builder
	env['BUILDERS']['HFST_SUBSTITUTE'] = substitute_builder
	env['BUILDERS']['HFST_SUBTRACT'] = subtract_builder
	env['BUILDERS']['HFST_DISJUNCT'] = disjunct_builder
	if env.Detect("hfst-calculate"):
		env['BUILDERS']['HFST_CALCULATE'] = calculate_builder
	else:
		env['BUILDERS']['HFST_CALCULATE'] = sfstpl2fst_builder
	env['BUILDERS']['HFST_CALCULATE'] = calculate_builder
	env['BUILDERS']['HFST_DETERMINIZE'] = determinize_builder
	env['BUILDERS']['HFST_FST2FST'] = fst2fst_builder
	env['BUILDERS']['HFST_FST2STRINGS'] = fst2strings_builder
	env['BUILDERS']['HFST_FST2TXT'] = fst2txt_builder
	env['BUILDERS']['HFST_MINIMIZE'] = minimize_builder
	env['BUILDERS']['HFST_NAME'] = name_builder
	env['BUILDERS']['HFST_PROJECT'] = project_builder
	env['BUILDERS']['HFST_PUSH_WEIGHTS'] = push_weights_builder
	env['BUILDERS']['HFST_REGEXP2FST'] = regexp2fst_builder
	env['BUILDERS']['HFST_REMOVE_EPSILONS'] = remove_epsilons_builder
	env['BUILDERS']['HFST_REVERSE'] = reverse_builder
	env['BUILDERS']['HFST_REPEAT'] = repeat_builder
	if env.Detect("hfst-sfstpl2fst"):
		env['BUILDERS']['HFST_SFSTPL2FST'] = sfstpl2fst_builder
	else:
		env['BUILDERS']['HFST_SFSTPL2FST'] = calculate_builder
	env['BUILDERS']['HFST_SFSTPL2FST'] = sfstpl2fst_builder
	env['BUILDERS']['HFST_TAIL'] = tail_builder
	env['BUILDERS']['HFST_TXT2FST'] = txt2fst_builder
	env['BUILDERS']['HFST_XFST'] = xfst_builder
	env['BUILDERS']['FOMA'] = xfst_builder
	env['BUILDERS']['HFST_COMPARE'] = compare_builder
	env['BUILDERS']['HFST_COMPOSE'] = compose_builder
	env['BUILDERS']['HFST_COMPOSE_INTERSECT'] = compose_intersect_builder
	env['BUILDERS']['HFST_CONCATENATE'] = concatenate_builder
	env['BUILDERS']['HFST_CONJUNCT'] = conjunct_builder
	env['BUILDERS']['HFST_STRINGS2FST'] = strings2fst_builder
	env.Append(SCANNERS = sfst_scan)
	env.Append(SCANNERS = xfst_scan)


#each SCons tool has to provide the exists() function.

def exists(env):
	return env.Detect("hfst-lookup")



